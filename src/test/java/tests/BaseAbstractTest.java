package tests;

import com.epam.steps.Steps;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * @author Nadzeya_Tsahelnik
 */
public abstract class BaseAbstractTest {

    public static Steps steps;
    WebDriver driver;
    public final String login1 = "alexdarkstalker232@gmail.com";
    public final String password1 = "samadura";
    public final String login2 = "nadzeya.tsahelnik@gmail.com";
    public final String password2 = "nadzeya46";
    public final String login3 = "user3forwebdr@gmail.com";
    public final String password3 = "time4fun!";

    @BeforeClass
    public void setUp() {
        steps = new Steps();
        steps.init();
    }

    @AfterClass
    public void stopBrowser() {
        steps.closeDriver();
    }
}
