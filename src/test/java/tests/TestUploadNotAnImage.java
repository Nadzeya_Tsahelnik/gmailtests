package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Nadzeya_Tsahelnik
 */
public class TestUploadNotAnImage extends BaseAbstractTest {

    private final String FILE_PATH = "file.txt";
    private final String ERROR_MESSAGE = "Error message don't appear";

    @Test
    public void testSettingsTheme() throws IOException, FileNotFoundException, AWTException {
        steps.logIn(login1, password1);
        steps.changeBackgroundImage(FILE_PATH);
        Assert.assertTrue(steps.isErrorMessageAppear(), ERROR_MESSAGE);
    }
}
