package tests;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Nadzeya_Tsahelnik
 */
public class TestChangeUserTheme extends BaseAbstractTest {

    private final String MESSAGE = "Something wrong with changing theme";
    private final String INFO_MESSAGE = "Test complete";
    final static Logger logger = Logger.getLogger(TestChangeUserTheme.class);

    @Test
    public void testChangeUserTheme() {
        steps.logIn(login1, password1);
        steps.changeUserTheme();
        Assert.assertTrue(steps.isBeachThemechange(), MESSAGE);
        logger.info(INFO_MESSAGE);
    }

}
