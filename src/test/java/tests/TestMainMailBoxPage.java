package tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;


/**
 *
 * @author Nadzeya_Tsahelnik
 */
public class TestMainMailBoxPage extends BaseAbstractTest {

    WebDriver driver;
    private final String THEME = "Message with attach";
    private final String FAIL_MESSAGE = "Something wrong";
    private final String SUCCESS_MESSAGE = "Success.Warning message that size of file is bigger than 25 mb. ";
    private final String FILE_PATH = "file.txt";
    private final int size = 1024 * 1024 * 26;
    final static Logger logger = Logger.getLogger(TestMainMailBoxPage.class);

    @BeforeMethod
    public void setUpMethod() throws Exception {
        steps.logIn(login1, password1);
    }

    @Test
    public void testAttachBigFile() throws IOException, AWTException {
        steps.attachFileBiggerThen25Mb(login1, THEME, FILE_PATH, size);
        Assert.assertTrue(steps.isWarningMessageAppeared(), FAIL_MESSAGE);
        logger.info(SUCCESS_MESSAGE);

    }
}
